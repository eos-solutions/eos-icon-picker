# EOS Icon picker
[![Open Source Love svg2](https://badges.frapsoft.com/os/v2/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)




## Installation

- Cloning the repo: `git clone https://gitlab.com/SUSE-UIUX/eos-icon-picker.git`

- Once the repository is cloned, run `npm i` to install the required packages to run this project.

- You should also have a .env file (refer `.env.template`). If you are working locally, the NODE_ENV should be `DEV` and if you want to test the analytics, put your mongodb uri (usually `mongodb://127.0.0.1:27017` locally) in `MONGO_URI`


## Running the project

To start the server, run `npm start` and your server will be started at `http://localhost:3131/` 

## Testing the API

You can use the postman collection `postman/eos-icons-picker.postman_collection.json` to test the API's for now.

To run postman test you will need to install newman:

`npm install -g newman`

<br>

# Learn more about the EOS Design System

* [EOS Design System](https://www.eosdesignsystem.com/)

* [EOS Icons](icons.eosdesignsystem.com/)

* [Follow us on Twitter](https://twitter.com/eosdesignsystem)

* [Join us in Slack](https://slack.eosdesignsystem.com)

# Our "thank you" section

### Tested for every browser in every device

Thanks to [Browserstack](https://www.browserstack.com) and their continuous contribution to open source projects, we continuously test the EOS to make sure all our features and components work perfectly fine in all browsers.
Browserstack helps us make sure our Design System also delivers a peace of mind to all developers and designers making use of our components and layout in their products.
