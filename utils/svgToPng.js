const fs = require('fs');
const svg2img = require('svg2img');
const tempDir = `${__dirname}/../temp/`;

convertSvgToPng = (iconName, pngSize, theme = 'svg') => {
  return new Promise((resolve, reject) => {
    if (!fs.existsSync(tempDir)) {
      fs.mkdirSync(tempDir);
    }
    svg2img(`${__dirname}/../${theme}/${iconName}.svg`, { width: pngSize, height: pngSize }, (error, buffer) => {
      if (error) {
        return reject();
      }
      fs.writeFileSync(`${__dirname}/../temp/${iconName}_${pngSize}.png`, buffer);
      resolve();
    });
  });
};

module.exports = convertSvgToPng;
